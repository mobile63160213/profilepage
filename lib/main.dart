import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.teal.shade800,
        )
    );
  }

  static ThemeData appThemeDark() {
    return  ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.black,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.amberAccent,
        )
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme==APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppbarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.nightlight),
          onPressed: (){
            setState(() {
              currentTheme==APP_THEME.DARK
                  ? currentTheme=APP_THEME.LIGHT
                  : currentTheme=APP_THEME.DARK;
            });
          }
        ),
      ),
    );
  }
}


Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
//ListTile
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title:Text("091-709-0512") ,
    subtitle:Text("mobile") ,
    trailing:IconButton(
      icon: Icon(Icons.message),
      // color: Colors.indigo.shade800,
      onPressed: (){},
    ) ,
  );
}
Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      // color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160213@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing: Text(""),
  );
}
Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("Chonburi, Thailand"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      // color: Colors.indigo.shade800,
      onPressed: () {},
    ),
  );
}

AppBar buildAppbarWidget(){
  return AppBar(
      backgroundColor: Colors.redAccent,
      leading: Icon(
        Icons.arrow_back,
        // color: Colors.lightBlueAccent,
      ),
      actions: <Widget>[
      IconButton(
      onPressed: () {},
  icon: Icon(Icons.star_border),
  color: Colors.greenAccent)
  ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 350,

            child: Image.network(
              "https://lh3.googleusercontent.com/chat_attachment/AJh6Fprb9w-4Ia3_wCyfONGKYsgI0x5oq5vYLkxNIgO77QLEj5J5viEJa2PmT4oLInLPOziZqkzhw-5ysP4Jfp4IaUI4x0F-kjEYHg7Uy5eZzA-JtO2aVf1KSYUI0YexapYvhKtQIBhrs8wzPOdIQLmhVTCB7J-McGFxUAAbMjR1L7i2TVbEJqG_Jq6VwW9lZZ_PXxWVCltdIOrDiEwZa70jMPwlZI_4cqrXTCy2NR1c4RvcTzrfgYcoXzI=w1920-h961",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Phoosanisa",
                    style: TextStyle(fontSize: 30),
                  ),
                )
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
            thickness: 0.8,
          ),
          Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Theme(
                data: ThemeData(
                    iconTheme: IconThemeData(
                      color: Colors.deepOrange,
                    )
                ),
                child: profileActionItems(),
              )
          ),
          Divider(
            color: Colors.grey,
            thickness: 0.5,
          ),
          mobilePhoneListTile(),
          // otherPhoneListTile(),
          Divider(
            color: Colors.grey,
            thickness: 0.3,
          ),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}
